package io.saagie.store.domain

import assertk.assertThat
import assertk.assertions.isEqualTo
import io.saagie.store.stock.Reference
import org.junit.jupiter.api.Test

class ShoppingBasketTest {
    @Test
    fun `should return price 0 for an empty shopping basket`() {
        val shoppingBasket = ShoppingBasket()
        val price = shoppingBasket.priceInEuroCents
        val expectedPrice = 0L
        assertThat(price).isEqualTo(expectedPrice)
    }

    @Test
    fun `should return price for a shopping basket with one item`() {
        val itemQuantity1 = ItemQuantity(
                DEFAULT_REFERENCE_1,
                4
        )
        val shoppingBasketItems = setOf(
                itemQuantity1
        )
        val shoppingBasket = ShoppingBasket(
                itemQuantities = shoppingBasketItems
        )
        val price = shoppingBasket.priceInEuroCents
        val expectedPrice = itemQuantity1.priceInEuroCents
        assertThat(price).isEqualTo(expectedPrice)
    }

    @Test
    fun `should return price for a shopping basket with multiple items`() {
        val itemQuantity1 = ItemQuantity(
                DEFAULT_REFERENCE_1,
                2
        )
        val itemQuantity2 = ItemQuantity(
                DEFAULT_REFERENCE_2,
                1
        )
        val itemQuantity3 = ItemQuantity(
                DEFAULT_REFERENCE_3,
                9
        )
        val shoppingBasketItems = setOf(
                itemQuantity1,
                itemQuantity2,
                itemQuantity3
        )
        val shoppingBasket = ShoppingBasket(
                itemQuantities = shoppingBasketItems
        )
        val price = shoppingBasket.priceInEuroCents
        val expectedPrice = itemQuantity1.priceInEuroCents + itemQuantity2.priceInEuroCents + itemQuantity3.priceInEuroCents
        assertThat(price).isEqualTo(expectedPrice)
    }

    companion object {
        private val DEFAULT_REFERENCE_1 = Reference(
                name = "Reference 1",
                priceInEuroCents = 180
        )
        private val DEFAULT_REFERENCE_2 = Reference(
                name = "Reference 2",
                priceInEuroCents = 5
        )
        private val DEFAULT_REFERENCE_3 = Reference(
                name = "Reference 3",
                priceInEuroCents = 77
        )
    }
}