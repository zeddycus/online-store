package io.saagie.store.domain


import assertk.assertThat
import assertk.assertions.isEqualTo
import io.saagie.store.stock.Reference
import org.junit.jupiter.api.Test

class ItemQuantityTest {
    @Test
    fun `should compute a correct price for a unitary quantity`() {
        val itemQuantity = ItemQuantity(
                reference = DEFAULT_REFERENCE,
                quantity = 1
        )
        val price = itemQuantity.priceInEuroCents
        val expectedPrice = DEFAULT_REFERENCE.priceInEuroCents
        assertThat(price).isEqualTo(expectedPrice)
    }

    @Test
    fun `should compute a correct price for a multiple quantity`() {
        val itemQuantity = ItemQuantity(
                reference = DEFAULT_REFERENCE,
                quantity = 4
        )
        val price = itemQuantity.priceInEuroCents
        val expectedPrice = DEFAULT_REFERENCE.priceInEuroCents * itemQuantity.quantity
        assertThat(price).isEqualTo(expectedPrice)
    }

    companion object {
        private val DEFAULT_REFERENCE = Reference(
                name = "Name",
                priceInEuroCents = 180
        )
    }
}