package io.saagie.store.domain

import arrow.core.Either
import assertk.assertThat
import assertk.assertions.isEqualTo
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.saagie.store.email.NotificationGateway
import io.saagie.store.payment.CreditCard
import io.saagie.store.payment.Payment
import io.saagie.store.payment.PaymentGateway
import io.saagie.store.stock.Command
import io.saagie.store.stock.Reference
import io.saagie.store.stock.Reservation
import io.saagie.store.stock.StockGateway
import org.junit.jupiter.api.Test
import java.util.*

class ShoppingBasketPaymentServiceTest {
    private val mockedPaymentGateway : PaymentGateway = mock()
    private val mockedNotificationGateway : NotificationGateway = mock()
    private val mockedStockGateway: StockGateway = mock()
    private val shoppingBasketPaymentService = ShoppingBasketPaymentService(
            paymentGateway = mockedPaymentGateway,
            stockGateway = mockedStockGateway,
            notificationGateway = mockedNotificationGateway
    )

    @Test
    fun `should return error if shopping basket is empty`() {
        val result = shoppingBasketPaymentService.payShoppingBasket(
                customer = Customer(email = "empty@mail.com", shoppingBasket = ShoppingBasket()),
                paymentMean = DEFAULT_PAYMENT_MEAN
        )
        val expected = Either.Left(EmptyShoppingBasket)
        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `should send command if enough stock and success payment`() {
        given {
            mockedPaymentGateway.pay(
                    priceInEuroCents = DEFAULT_SHOPPING_BASKET.priceInEuroCents,
                    paymentMean = DEFAULT_PAYMENT_MEAN
            )
        }.willReturn(
                Either.Right(Payment(uuid = DEFAULT_PAYMENT_UUID))
        )

        given {
            mockedStockGateway.reserveStock(DEFAULT_SHOPPING_BASKET.itemQuantities)
        }.willReturn(
                Either.Right(DEFAULT_RESERVATION)
        )
        given {
            mockedStockGateway.sendReservation(customer = DEFAULT_CUSTOMER, reservation = DEFAULT_RESERVATION)
        }.willReturn(
                DEFAULT_COMMAND
        )

        val result = shoppingBasketPaymentService.payShoppingBasket(
                customer = DEFAULT_CUSTOMER,
                paymentMean = DEFAULT_PAYMENT_MEAN
        )
        val expected = Either.Right(DEFAULT_COMMAND)

        verify(mockedStockGateway, times(1)).reserveStock(DEFAULT_SHOPPING_BASKET.itemQuantities)
        verify(mockedPaymentGateway, times(1)).pay(
                priceInEuroCents = DEFAULT_SHOPPING_BASKET.priceInEuroCents,
                paymentMean = DEFAULT_PAYMENT_MEAN
        )
        verify(mockedStockGateway, times(1))
                .sendReservation(customer = DEFAULT_CUSTOMER, reservation = DEFAULT_RESERVATION)
        verify(mockedNotificationGateway, times(1)).notifyCommandInPreparation(customer = DEFAULT_CUSTOMER, command = DEFAULT_COMMAND)
        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun `should notify stock issue if not enough stock`() {
        val missingReferences = setOf(REFERENCE_1)
        val expectedError = Either.Left(NotEnoughStock(missingReferences))

        given { mockedStockGateway.reserveStock(DEFAULT_SHOPPING_BASKET.itemQuantities) }
                .willReturn(expectedError)

        val result = shoppingBasketPaymentService.payShoppingBasket(
                customer = DEFAULT_CUSTOMER,
                paymentMean = DEFAULT_PAYMENT_MEAN
        )

        verify(mockedStockGateway, times(1)).reserveStock(DEFAULT_SHOPPING_BASKET.itemQuantities)
        verify(mockedNotificationGateway, times(1))
                .notifyNotEnoughStock(customer = DEFAULT_CUSTOMER, missingStock = missingReferences)
        assertThat(result).isEqualTo(expectedError)
    }

    @Test
    fun `should notify failure payment if payment failed`() {
        given {
            mockedStockGateway.reserveStock(DEFAULT_SHOPPING_BASKET.itemQuantities)
        }.willReturn(
                Either.Right(DEFAULT_RESERVATION)
        )
        given {
            mockedPaymentGateway.pay(
                    priceInEuroCents = DEFAULT_SHOPPING_BASKET.priceInEuroCents,
                    paymentMean = DEFAULT_PAYMENT_MEAN
            )
        }.willReturn(
                Either.Left(DEFAULT_PAYMENT_FAILURE)
        )

        val result = shoppingBasketPaymentService.payShoppingBasket(customer = DEFAULT_CUSTOMER, paymentMean = DEFAULT_PAYMENT_MEAN)

        verify(mockedStockGateway, times(1)).reserveStock(DEFAULT_SHOPPING_BASKET.itemQuantities)
        verify(mockedPaymentGateway, times(1)).pay(
                priceInEuroCents = DEFAULT_SHOPPING_BASKET.priceInEuroCents,
                paymentMean = DEFAULT_PAYMENT_MEAN
        )
        verify(mockedStockGateway, times(1)).cancelReservation(DEFAULT_RESERVATION)
        assertThat(result).isEqualTo(Either.Left(DEFAULT_PAYMENT_FAILURE))
    }

    companion object {
        private val DEFAULT_PAYMENT_MEAN = CreditCard(
                type = CreditCard.Type.VISA,
                code = "1234567812345678",
                expirationMonth = 1,
                expirationYear = 1,
                verificationCode = "123"
        )
        private val DEFAULT_PAYMENT_UUID = UUID.randomUUID()
        private val DEFAULT_RESERVATION_UUID = UUID.randomUUID()
        private val DEFAULT_COMMAND_UUID = UUID.randomUUID()
        private val REFERENCE_1 = Reference("1", 40)
        private val REFERENCE_2 = Reference("2", 1)
        private val REFERENCE_3 = Reference("3", 340)
        private val REFERENCE_4 = Reference("4", 1540)
        private val DEFAULT_SHOPPING_BASKET = ShoppingBasket(
                setOf(
                        ItemQuantity(reference = REFERENCE_1, quantity = 1L),
                        ItemQuantity(reference = REFERENCE_2, quantity = 2L),
                        ItemQuantity(reference = REFERENCE_3, quantity = 3L),
                        ItemQuantity(reference = REFERENCE_4, quantity = 4L)
                )
        )
        private val DEFAULT_CUSTOMER = Customer(email = "email@default.com", shoppingBasket = DEFAULT_SHOPPING_BASKET)
        private val DEFAULT_RESERVATION = Reservation(uuid = DEFAULT_RESERVATION_UUID, quantities = DEFAULT_SHOPPING_BASKET.itemQuantities)
        private val DEFAULT_COMMAND = Command(uuid = DEFAULT_COMMAND_UUID)
        private val DEFAULT_PAYMENT_FAILURE = PaymentFailure(message = "Invalid card")
    }
}