package io.saagie.store.email

import io.saagie.store.domain.Customer
import io.saagie.store.stock.Command
import io.saagie.store.stock.Reference
import io.saagie.store.domain.PaymentFailure

interface NotificationGateway {
    fun notifyErrorDuringPayment(customer: Customer, paymentFailure : PaymentFailure)
    fun notifyCommandInPreparation(customer: Customer, command: Command)
    fun notifyNotEnoughStock(customer: Customer, missingStock : Set<Reference>)
}