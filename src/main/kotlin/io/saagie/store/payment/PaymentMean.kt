package io.saagie.store.payment

sealed class PaymentMean

data class CreditCard(
        val type: Type,
        val code: String,
        val expirationMonth: Int,
        val expirationYear: Int,
        val verificationCode: String
) : PaymentMean() {
    enum class Type {
        VISA
    }
}
