package io.saagie.store.payment

import arrow.core.Either
import io.saagie.store.domain.PaymentFailure

interface PaymentGateway {
    fun pay(priceInEuroCents: Long, paymentMean: PaymentMean): Either<PaymentFailure, Payment>
}