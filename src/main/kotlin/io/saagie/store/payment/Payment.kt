package io.saagie.store.payment

import java.util.*

data class Payment(val uuid: UUID)