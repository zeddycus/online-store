package io.saagie.store.stock

data class Reference(
        val name: String,
        val priceInEuroCents: Long
)
