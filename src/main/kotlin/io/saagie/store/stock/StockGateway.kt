package io.saagie.store.stock

import arrow.core.Either
import io.saagie.store.domain.Customer
import io.saagie.store.domain.ItemQuantity
import io.saagie.store.domain.NotEnoughStock


interface StockGateway {
    fun cancelReservation(reservation: Reservation)
    fun sendReservation(customer: Customer, reservation: Reservation): Command
    fun reserveStock(itemQuantities: Set<ItemQuantity>): Either<NotEnoughStock, Reservation>
}