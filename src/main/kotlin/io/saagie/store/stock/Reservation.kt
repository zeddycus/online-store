package io.saagie.store.stock

import io.saagie.store.domain.ItemQuantity
import java.util.*

data class Reservation(
        val uuid: UUID,
        val quantities: Set<ItemQuantity>
)