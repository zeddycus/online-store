package io.saagie.store.domain

import io.saagie.store.stock.Reference

data class ItemQuantity(
        val reference: Reference,
        val quantity: Long
) {
    val priceInEuroCents : Long = quantity * reference.priceInEuroCents
}
