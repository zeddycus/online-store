package io.saagie.store.domain

import io.saagie.store.stock.Reference

sealed class Error

data class NotEnoughStock(val missingReferences: Set<Reference>) : Error()
data class PaymentFailure(val message: String): Error()
object EmptyShoppingBasket: Error()