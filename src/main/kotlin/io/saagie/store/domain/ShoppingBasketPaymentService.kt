package io.saagie.store.domain

import arrow.core.Either
import io.saagie.store.email.NotificationGateway
import io.saagie.store.payment.PaymentGateway
import io.saagie.store.payment.PaymentMean
import io.saagie.store.stock.Command
import io.saagie.store.stock.StockGateway

class ShoppingBasketPaymentService(
        private val notificationGateway: NotificationGateway,
        private val stockGateway: StockGateway,
        private val paymentGateway: PaymentGateway
) {
    fun payShoppingBasket(customer: Customer, paymentMean: PaymentMean): Either<Error, Command> {
        if (customer.shoppingBasket.isEmpty()) {
            return Either.left(EmptyShoppingBasket)
        }
        return when (
            val reservation = stockGateway.reserveStock(customer.shoppingBasket.itemQuantities)) {
            is Either.Left -> {
                notificationGateway.notifyNotEnoughStock(
                        customer = customer,
                        missingStock = reservation.a.missingReferences
                )
                Either.left(reservation.a)
            }
            is Either.Right -> {
                when (val payment = paymentGateway.pay(
                        priceInEuroCents = customer.shoppingBasket.priceInEuroCents,
                        paymentMean = paymentMean
                )) {
                    is Either.Left -> {
                        stockGateway.cancelReservation(reservation.b)
                        notificationGateway.notifyErrorDuringPayment(customer = customer, paymentFailure = payment.a)
                        Either.left(payment.a)
                    }
                    is Either.Right -> {
                        val command = stockGateway.sendReservation(customer = customer, reservation = reservation.b)
                        notificationGateway.notifyCommandInPreparation(customer = customer, command = command)
                        Either.right(command)
                    }
                }
            }
        }
    }
}