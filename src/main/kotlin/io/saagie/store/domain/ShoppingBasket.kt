package io.saagie.store.domain

data class ShoppingBasket(
        val itemQuantities: Set<ItemQuantity> = emptySet()
) {
    fun isEmpty(): Boolean {
        return itemQuantities.isEmpty()
    }

    val priceInEuroCents : Long = itemQuantities.map { it.priceInEuroCents }.sum()
}