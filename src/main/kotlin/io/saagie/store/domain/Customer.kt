package io.saagie.store.domain

data class Customer(
        val email: String,
        val shoppingBasket: ShoppingBasket = ShoppingBasket()
)