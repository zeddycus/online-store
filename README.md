# online-store

Online-store is a technical test. The purpose is to create a method to validate and pay an online shopping basket.

## Run
For run the tests :
```
mvn clean test
```

## Implementation choice

### Technical choice

Because I'm used to working with, I choose maven for project management.
I tried to work with Saagie technologies, then kotlin with arrow (first-use).
I used for test: mockito, junit5 and assertk.

### Domain choice
 
#### Shopping Basket management

Into ShoppingBasketPaymentService, the method 
`payShoppingBasket(Customer, PaymentMean)` is the asked method.
I choose that the shopping basket management (creation, update or empty after the payment) will be executed 
into an upper level and don't manage this part.

#### Currency

I start to implement Price class with different currency but find quickly
that the currency change to do basic operation could be very complicated the value, the values being variable.
I fixed the currency arbitrarily in euro.

The test doesn't give any clue about the need for different currencies, I suppose I can remove it and avoid unnecessary complexity.

#### Payment means

I choose the easy way too to implement the different payment means with a sealed class.
I considered by the given explanation that the payment will be server-side and will work completely 
by the PaymentGateway method "pay" without a two-step action with a bill creation and external communication.
